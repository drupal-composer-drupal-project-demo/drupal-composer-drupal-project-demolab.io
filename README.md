# [drupal-composer-drupal-project-demo.gitlab.io](https://drupal-composer-drupal-project-demo.gitlab.io/)

## Some Drupal modules to consider
* [gutenberg](https://www.drupal.org/project/gutenberg) UI (from WordPress)
* [ludwig](https://www.drupal.org/project/ludwig) Provides a manual alternative to Composer.
* [migrate_tools](https://www.drupal.org/project/migrate_tools) DB
* [node_clone: Drupal 8 Porting](https://www.drupal.org/project/contrib_tracker/issues/2584487)
  * [entity_clone](https://www.drupal.org/project/entity_clone)
  * [quick_node_clone](https://www.drupal.org/project/quick_node_clone)
  * [replicate](https://www.drupal.org/project/replicate)
* [permissions_by_term](https://www.drupal.org/project/permissions_by_term) Authorization
* [scheduler](https://www.drupal.org/project/scheduler)
  Schedule nodes to be published and unpublished at specified dates and times in the future.
* [simplenews](https://www.drupal.org/project/simplenews)